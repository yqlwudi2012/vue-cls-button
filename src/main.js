import Vue from 'vue'
import App from './App.vue'
import vueBtn from './lib/index.js'
Vue.use(vueBtn);

new Vue({
  el: '#app',
  render: h => h(App)
})
