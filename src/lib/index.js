import vueBtn from './vue-cls-btn.vue';
import vueRdBtn from './vue-rd-btn.vue';

const clsBtn={
  install(Vue,options){
    Vue.component(vueBtn.name,vueBtn);
    Vue.component(vueRdBtn.name,vueRdBtn);
    if (typeof window !== 'undefined' && window.Vue) { window.Vue.use(vueBtn);window.Vue.use(vueRdBtn); }
  }
}
export default clsBtn;
